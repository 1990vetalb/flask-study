from flask import Flask, url_for, render_template, Markup, request, make_response, abort, redirect, jsonify, session, flash, json
from werkzeug.utils import secure_filename


app = Flask(__name__)

#секректый ключ который используэться для шифрования
app.secret_key = '\rL\x7fp\x0e\xf6s(6Gj\xbeF\xf08\xfa'

@app.route('/')
def index():
    resp = make_response(render_template('index.html'))
    resp.set_cookie('username', 'the username')

    return resp


@app.route('/redirect')
def redirect_link():
    return redirect(url_for('abort_link'))


@app.route('/abort')
def abort_link():
    abort(404)


#перехватчик ошибок указує какую ошибку хотим отловить і можна добавляти хедери
@app.errorhandler(404)
def page_not_found(error):

    app.logger.debug('A value for debugging')
    app.logger.warning('A warning occurred (%d apples)', 42)
    app.logger.error('An error occurred')

    resp = make_response(render_template('page_not_found.html', error=error), 404)
    resp.headers['X-Something'] = 'A value'
    return resp


@app.route('/me')
def me_api():
    return {
        'name': 'name',
        'theme': 'theme',
        "image": url_for("user_image", filename='upload/mouse.jpg'),

    }


@app.route('/user-image')
def user_image(filename):
    return filename


@app.route('/news')
def news():
    #добавление сесии
    session['test-session'] = 'hellooo'

    flash('messageeee', 'cat1')
    flash('messageeee2', 'cat1')
    flash('messageeee3', 'cat2')

    return render_template('news.html')



@app.route('/news/<int:id>')
def single_news(id):
    #удаление сесии
    session.pop('test-session', None)
    return 'news ID: {0}'.format(id)


@app.route('/page/<path:subpage>')
def show_subpage(subpage):
    return 'subpage {0}'.format(subpage)

# Если нужно указывать в конце слеш то можна в роуте так указать и если пользователь зайдет без слеша его редиректнет на страницу со слешом
@app.route('/projects/')
def projects():
    return 'The project page'


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.form['username'],
                       request.form['password']):
            return log_the_user_in(request.form['username'])
        else:
            error = 'Invalid username/password'
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    return render_template('login.html', error=error)


@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    searchword = request.args.get('key', '')
    return render_template('hello.html', name=name, searchword=searchword)


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        #Можна узнать какие данные есть у обьекта
        #print(request.files['file'].__dict__)
        filename = request.files['file'].filename
        f = request.files['file']
        print(secure_filename(f.filename))
        f.save('/var/www/flask-learn/upload/' + secure_filename(f.filename))

    return render_template('upload.html')








#Создание силки
#url_for('static', filename='style.css')




# with app.test_request_context('/hello', method='POST'):
#     assert request.path == '/hello'
#     assert request.method == 'GET'


# with app.test_request_context():
#     url_for('static', filename='style.css')
#
# print(Markup('asdasd %s') % 'ddddddddddd')
